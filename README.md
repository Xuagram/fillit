# Fillit

This is the last project at 42 school before specialization into one of the branch : Unix system, graphic or algorithm. 
You can find more details on the subject in this repository in the file : `fillit.fr.pdf`

The program takes a file as parameter, which contains a list of Tetriminos, and arrange them in order to create the smallest square possible.

This project was written in C and uses binary masks to optimize time of execution and uses short int type of variable to lighten memory ! 

# Credits

Project realized with another team member, temehenn.

# How to run

This program shell can be compiled on all unix system.

- Clone this repository : `git clone https://gitlab.com/Xuagram/fillit.git`

- Change directory into the new directory and run make
```
cd fillit
make 
```

- Run 

`./fillit exemple.fillit`

- You can create your own Tetriminos file following this example, and give it to the binary :

`cat exemple.fillit`

Enjoy !
