/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_filled_map.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/28 12:37:44 by mahaffne          #+#    #+#             */
/*   Updated: 2019/01/11 13:29:42 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

int	is_filled_map(unsigned short int map_bits[16])
{
	unsigned short int	mask;
	int					i;

	mask = 0xFFFF;
	i = 0;
	while (i < 16)
	{
		if (mask & map_bits[i])
			return (1);
		i++;
	}
	return (0);
}
