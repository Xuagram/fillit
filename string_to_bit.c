/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string_to_bit.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 11:20:28 by temehenn          #+#    #+#             */
/*   Updated: 2019/01/11 13:30:14 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

unsigned short int	strtobit(char *str)
{
	unsigned int		power;
	unsigned short int	result;
	int					i;

	power = 1;
	result = 0;
	i = ft_strlen(str) - 1;
	while (i >= 0)
	{
		if (str[i] == '#')
			result = result + power;
		power = power * 2;
		i--;
	}
	return (result);
}
