/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_result.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/21 09:45:22 by temehenn          #+#    #+#             */
/*   Updated: 2019/01/11 13:34:20 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

void	print_result(char printed_map[16][17], int size)
{
	int	i;
	int	j;

	i = 0;
	while (i < size && i < 16)
	{
		j = 0;
		while (j < size && j < 17)
			ft_putchar(printed_map[i][j++]);
		ft_putchar('\n');
		i++;
	}
}
