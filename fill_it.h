/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_it.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 14:56:29 by temehenn          #+#    #+#             */
/*   Updated: 2019/01/14 17:32:15 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILL_IT_H
# define FILL_IT_H
# include "libft.h"
# include <stdlib.h>

typedef struct				s_tetra
{
	unsigned short int		tetra[27][16];
	unsigned short int		tetra_back[27][16];
	int						nb_tetra;
}							t_tetra;

typedef	struct				s_vellada
{
	int						line;
	unsigned short int		clear_tetra[16];
	char					letter;
}							t_vellada;

typedef struct				s_map
{
	unsigned short int		map[16];
	char					printed_map[16][17];
	int						size;
	int						rsize;
	t_vellada				clear;
	unsigned short int		result[16];
}							t_map;

int							ft_int_sqr(int nb);
void						print_result(char printed_map[16][17], int size);
int							get_next_line(int fd, char **line);
int							is_filled_map(unsigned short int map_bits[16]);
int							put_tetra(t_map *map, unsigned short int tetra[16]);
unsigned short int			mask_generator(int size);
int							is_overlap(t_map *map,
										unsigned short int tetra[16]);
void						init_tab(unsigned short int tetra[27][16],
										t_map *map);
unsigned short int			strtobit(char *str);
void						make_mask(unsigned short int mask[2]);
int							check_shape(unsigned short int *tetra);
int							four_line(unsigned int *nb_line, t_tetra *tetra,
									int *ret, int fd);
int							ft_usage(int argc);
int							check_bits(t_tetra *tetra);
int							count_block(unsigned short int tetra[16],
							int nb_bits, int map_size);
int							fill_tetra(t_tetra *tetra, unsigned int fd);
int							check_line(char *line);
void						to_the_left(unsigned short int tetra[16]);
void						to_the_top(unsigned short int tetra[16]);
void						to_left(unsigned short int tetra[16]);
void						velleda_tetra(unsigned short int tetra[16],
									unsigned short int map[16]);
int							try_square(t_map *map, t_tetra *tetra,
									int nb, int shift);
void						fill_it(t_map *map, t_tetra *tetra);
void						fill_map_bit(t_map *map,
									unsigned short int tetra[16]);
void						fill_it_all(t_map *map,
							unsigned short int tetra[16], char letter);
int							transe_tetra(unsigned short int tetra[16],
											int size_map);
void						translate(unsigned short int tetra[16], int shift,
										int size);
void						fill_printed_map(t_tetra *tetra, t_map *map);
void						init_printed_map(char printed_map[16][17]);
int							side_check(t_map *map,
							unsigned short int tetra[16]);
#endif
