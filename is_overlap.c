/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_overlap.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/19 13:15:47 by temehenn          #+#    #+#             */
/*   Updated: 2019/01/14 17:45:16 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

int			is_overlap(t_map *map, unsigned short int tetra[16])
{
	int	i;

	i = 0;
	while (i < map->rsize + 1)
	{
		if (map->map[i] & tetra[i])
			return (0);
		i++;
	}
	return (1);
}

static void	yolo_mask(unsigned short int mask, t_map *map)
{
	int	i;

	i = 0;
	while (i < map->rsize)
	{
		map->map[i] ^= mask;
		i++;
	}
}

int			side_check(t_map *map, unsigned short int tetra[16])
{
	unsigned short int	mask;

	mask = 0x8000 >> map->rsize;
	yolo_mask(mask, map);
	if (is_overlap(map, tetra))
	{
		yolo_mask(mask, map);
		return (1);
	}
	yolo_mask(mask, map);
	return (0);
}
