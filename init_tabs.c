/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_tabs.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/19 12:47:46 by temehenn          #+#    #+#             */
/*   Updated: 2019/01/11 13:23:51 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

static void	init_tetra(unsigned short int tetra[27][16])
{
	int	i;

	i = 0;
	while (i < 27)
	{
		ft_bzero(tetra[i], 32);
		i++;
	}
}

static void	init_map(unsigned short int map[16])
{
	ft_bzero(map, 16 * 2);
}

void		init_printed_map(char printed_map[16][17])
{
	int	i;
	int	j;

	i = 0;
	while (i < 16)
		ft_bzero(printed_map[i++], 17);
	i = 0;
	while (i < 16)
	{
		j = 0;
		while (j < 16)
		{
			printed_map[i][j] = '.';
			j++;
		}
		i++;
	}
}

void		init_tab(unsigned short int tetra[27][16], t_map *map)
{
	init_tetra(tetra);
	init_map(map->map);
}
