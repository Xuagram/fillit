# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: temehenn <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/01/16 13:28:49 by temehenn          #+#    #+#              #
#    Updated: 2019/01/16 13:29:07 by temehenn         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME=fillit
CC=gcc
FLAGS= -Wall -Wextra -Werror
SRC= fill_it.c\
	 ft_int_sqr.c\
	 is_overlap.c\
	 string_to_bit.c\
	 check_tetra.c\
	 print_result.c\
	 to_left.c\
	 clear_tetra.c\
	 fill_map.c\
	 init_tabs.c\
	 main.c\
	 put_tetra.c\
	 transe_tetra.c\
	 fill_tetra.c\
	 is_filled_map.c\
	 try_square.c\

LIBFT= ./libft

OBJ= $(SRC:.c=.o)

all:	$(NAME)

$(NAME):
		make -C ./libft
		$(CC) $(FLAGS) -c $(SRC) -I libft/includes
		$(CC) -o $(NAME) $(OBJ) ./libft/libft.a

clean:
		make clean -C $(LIBFT)
		rm -rf $(OBJ)

fclean: clean
		make fclean -C $(LIBFT)
		rm -rf $(NAME)

re: fclean all

.PHONY: all clean fclean re
