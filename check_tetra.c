/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_tetra.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/14 15:40:04 by mahaffne          #+#    #+#             */
/*   Updated: 2019/01/16 16:10:10 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

void	make_mask(unsigned short int mask[20])
{
	mask[0] = 0xCC00;
	mask[1] = 0xE800;
	mask[2] = 0xE200;
	mask[3] = 0x8E00;
	mask[4] = 0x2E00;
	mask[5] = 0x88C0;
	mask[6] = 0xC440;
	mask[7] = 0xC880;
	mask[8] = 0x44C0;
	mask[9] = 0x4E00;
	mask[10] = 0xE400;
	mask[11] = 0x8C80;
	mask[12] = 0x4C40;
	mask[13] = 0xC600;
	mask[14] = 0x6C00;
	mask[15] = 0x8C40;
	mask[16] = 0x4C80;
	mask[17] = 0x8888;
	mask[18] = 0xF000;
}

int		count_block(unsigned short int tetra[16], int nb_bits, int map_size)
{
	int					i;
	int					j;
	int					count;
	unsigned short int	mask;
	unsigned short int	tmp_tetra[16];

	ft_memcpy(tmp_tetra, tetra, 32);
	count = 0;
	mask = 0x8000;
	i = 0;
	while (i < map_size)
	{
		j = 0;
		while (j < map_size)
		{
			if (mask & tmp_tetra[i])
				count++;
			tmp_tetra[i] <<= 1;
			j++;
		}
		i++;
	}
	return (count != nb_bits) ? 0 : 1;
}

int		check_shape(unsigned short int *tetra)
{
	int					i;
	unsigned short int	mask[20];
	unsigned short int	mask_1;
	unsigned short int	mask_2;
	unsigned short int	mask_3;

	i = 0;
	make_mask(mask);
	while (i < 20)
	{
		mask_1 = mask[i];
		mask_2 = mask[i];
		mask_3 = mask[i];
		mask[i] = mask[i] >> 12 << 12;
		mask_1 = mask_1 >> 8 << 12;
		mask_2 = mask_2 >> 4 << 12;
		mask_3 = mask_3 << 12;
		if (tetra[0] == mask[i] && tetra[1] == mask_1
				&& tetra[2] == mask_2 && tetra[3] == mask_3)
			return (1);
		i++;
	}
	return (-1);
}

int		ft_usage(int argc)
{
	if (argc != 2)
	{
		ft_putendl("usage : fillit file");
		return (-1);
	}
	return (1);
}

int		check_line(char *line)
{
	int i;

	i = 0;
	if (ft_strlen(line) != 4)
		return (-1);
	while (i < 4)
	{
		if (!(line[i] == '#') && !(line[i] == '.'))
			return (-1);
		i++;
	}
	return (1);
}
