/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_tetra.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 13:09:59 by mahaffne          #+#    #+#             */
/*   Updated: 2019/01/14 16:44:52 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

int		four_line(unsigned int *nb_line, t_tetra *tetra, int *ret, int fd)
{
	char	*line;
	int		num_line;

	line = NULL;
	num_line = -1;
	while (++num_line < 4)
	{
		*ret = get_next_line(fd, &line);
		if (*ret == 0 || check_line(line) == -1 || *ret == -1)
		{
			ft_strdel(&line);
			return (-1);
		}
		tetra->tetra[tetra->nb_tetra][num_line] = strtobit(line);
		ft_strdel(&line);
		(*nb_line)++;
	}
	*nb_line = *nb_line + 1;
	*ret = get_next_line(fd, &line);
	if ((*ret != 0 && ft_strcmp(line, "\0")) || *ret == -1)
		return (-1);
	ft_strdel(&line);
	if (*ret == 0)
		return (1);
	return (1);
}

int		check_bits(t_tetra *tetra)
{
	if (!count_block(tetra->tetra[tetra->nb_tetra], 4, 16))
		return (-1);
	to_the_left((unsigned short int *)tetra->tetra[tetra->nb_tetra]);
	if (check_shape(tetra->tetra[tetra->nb_tetra]) == -1)
		return (-1);
	if (!(tetra->tetra[tetra->nb_tetra]))
		return (-1);
	tetra->nb_tetra += 1;
	return (1);
}

int		fill_tetra(t_tetra *tetra, unsigned int fd)
{
	int				ret;
	unsigned int	nb_line;

	nb_line = 0;
	ret = 1;
	while ((ret > 0) && (nb_line <= 130))
	{
		if (nb_line == 130 || four_line(&nb_line, tetra, &ret, fd) == -1 ||
				check_bits(tetra) == -1)
		{
			ft_putendl("error");
			return (-1);
		}
	}
	ft_memcpy(tetra->tetra_back, tetra->tetra, 864);
	if (close(fd) == -1)
		exit(0);
	return (1);
}
