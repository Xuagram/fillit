/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_map.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/19 13:29:17 by temehenn          #+#    #+#             */
/*   Updated: 2019/01/11 13:23:10 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

static void	get_pos(unsigned short int tetra[16],
					char printed_map[16][17], char letter)
{
	unsigned short int	mask;
	int					x;
	int					y;

	mask = 0x8000;
	x = 0;
	while (x < 16)
	{
		y = 0;
		while (y < 17)
		{
			if (mask & tetra[x])
				printed_map[x][y] = letter;
			tetra[x] <<= 1;
			y++;
		}
		x++;
	}
}

void		fill_printed_map(t_tetra *tetra, t_map *map)
{
	int		i;
	char	letter;
	char	printed_map[16][17];

	i = 0;
	letter = 'A';
	init_printed_map(printed_map);
	while (i < tetra->nb_tetra)
	{
		get_pos(tetra->tetra_back[i], printed_map, letter + i);
		i++;
	}
	print_result(printed_map, map->rsize);
}

void		fill_map_bit(t_map *map, unsigned short int tetra[16])
{
	int i;

	i = 0;
	while (i < 16)
	{
		map->map[i] ^= tetra[i];
		i++;
	}
}
