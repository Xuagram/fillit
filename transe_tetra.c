/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   transe_tetra.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/07 17:09:36 by temehenn          #+#    #+#             */
/*   Updated: 2019/01/11 13:26:39 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

static void	all_to_left(unsigned short int tetra[16])
{
	unsigned short int	mask;
	int					i;
	int					ret;

	mask = 0x8000;
	ret = 0;
	while (ret == 0)
	{
		i = 0;
		while (ret == 0 && i < 16)
		{
			ret = mask & tetra[i];
			i++;
		}
		i = 0;
		while (ret == 0 && i < 16)
		{
			tetra[i] <<= 1;
			i++;
		}
	}
}

static int	side_detector(unsigned short int tetra[16], int size)
{
	unsigned short int	mask;
	int					i;

	i = 0;
	mask = 0x8000 >> (size - 1);
	while (i < size)
	{
		if (tetra[i] & mask)
			return (0);
		i++;
	}
	return (1);
}

static void	height_translate(unsigned short int tetra[16])
{
	int i;

	i = 15;
	all_to_left(tetra);
	while (i - 1 >= 0)
	{
		tetra[i] = tetra[i - 1];
		i--;
	}
	tetra[0] = 0;
}

static int	height_30_detector(unsigned short int tetra[16], int size_map)
{
	unsigned short int mask;

	mask = 0xFFFF;
	if (tetra[size_map - 1] & mask)
		return (0);
	return (1);
}

int			transe_tetra(unsigned short int tetra[16], int size_map)
{
	if (side_detector(tetra, size_map))
	{
		translate(tetra, 1, size_map);
		return (1);
	}
	else if (height_30_detector(tetra, size_map))
	{
		height_translate(tetra);
		return (1);
	}
	return (0);
}
