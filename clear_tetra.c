/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clear_tetra.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/27 14:13:46 by mahaffne          #+#    #+#             */
/*   Updated: 2019/01/11 13:28:03 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

void	velleda_tetra(unsigned short int clear_tetra[16],
						unsigned short int map[16])
{
	int i;

	i = 0;
	while (i < 16)
	{
		map[i] ^= clear_tetra[i];
		i++;
	}
}
