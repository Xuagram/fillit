/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   try_square.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/26 17:48:29 by temehenn          #+#    #+#             */
/*   Updated: 2019/01/14 17:29:28 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

int	try_square(t_map *map, t_tetra *tetra, int nb, int shift)
{
	int ret;

	if (nb == tetra->nb_tetra && is_filled_map(map->map))
		return (1);
	while ((ret = put_tetra(map, tetra->tetra_back[nb])) != 1)
	{
		if (transe_tetra(tetra->tetra_back[nb], map->rsize) == 0
				&& ft_memcpy(tetra->tetra_back[nb], tetra->tetra[nb], 32))
			return (2);
		shift++;
	}
	if (ret == 1)
	{
		ret = try_square(map, tetra, nb + 1, 0);
		if (ret == 0 || ret == 1)
			return (ret);
		velleda_tetra(tetra->tetra_back[nb], map->map);
		if (transe_tetra(tetra->tetra_back[nb], map->rsize) == 1)
			if ((ret = try_square(map, tetra, nb, shift + 1)) == 1 || ret == 0)
				return (ret);
	}
	ft_memcpy(tetra->tetra_back[nb], tetra->tetra[nb], 32);
	return (2);
}
