/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 15:28:53 by mahaffne          #+#    #+#             */
/*   Updated: 2019/01/14 16:00:25 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"
#include <fcntl.h>

int		main(int argc, char **argv)
{
	int				fd;
	t_tetra			tetra;
	t_map			map;

	tetra.nb_tetra = 0;
	init_tab(tetra.tetra, &map);
	if (ft_usage(argc) == -1)
		exit(0);
	if ((fd = open(argv[1], O_RDONLY)) == -1)
	{
		ft_putendl("error");
		exit(0);
	}
	if (fill_tetra(&tetra, fd) == -1)
		exit(0);
	fill_it(&map, &tetra);
	return (0);
}
