/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put_tetra.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/26 13:28:04 by temehenn          #+#    #+#             */
/*   Updated: 2019/01/14 17:45:44 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

void		translate(unsigned short int tetra[16], int shift, int size)
{
	int i;

	i = 0;
	while (i < size)
		tetra[i++] >>= shift;
}

static int	is_side_ok(t_map *map, unsigned short int tetra[16])
{
	map->map[map->rsize] = 0xFFFF;
	if (is_overlap(map, tetra))
	{
		map->map[map->rsize] = 0x0000;
		return (1);
	}
	map->map[map->rsize] = 0x0000;
	return (0);
}

int			put_tetra(t_map *map, unsigned short int tetra[16])
{
	if (side_check(map, tetra) && is_side_ok(map, tetra))
	{
		fill_map_bit(map, tetra);
		return (1);
	}
	return (0);
}
