/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   to_left.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/21 18:14:32 by mahaffne          #+#    #+#             */
/*   Updated: 2019/01/11 13:34:42 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

void	to_the_top(unsigned short int tetra[16])
{
	unsigned short int mask_line;

	mask_line = 0b1111000000000000;
	while (!(mask_line & tetra[0]))
	{
		tetra[0] = tetra[1];
		tetra[1] = tetra[2];
		tetra[2] = tetra[3];
		tetra[3] = 0;
	}
}

void	to_left(unsigned short int tetra[16])
{
	unsigned short int mask_col;

	mask_col = 0b1000000000000000;
	while (!(mask_col & tetra[0]) && !(mask_col & tetra[1])
			&& !(mask_col & tetra[2]) && !(mask_col & tetra[3]))
	{
		tetra[0] <<= 1;
		tetra[1] <<= 1;
		tetra[2] <<= 1;
		tetra[3] <<= 1;
	}
}

void	to_the_left(unsigned short int tetra[16])
{
	to_left(tetra);
	to_the_top(tetra);
}
