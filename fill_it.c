/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_it.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/27 11:55:20 by mahaffne          #+#    #+#             */
/*   Updated: 2019/01/14 17:28:41 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fill_it.h"

int		size_square(int nb)
{
	int	result;

	result = 2;
	while (result * result < nb)
		result++;
	return (result * result);
}

void	fill_it(t_map *map, t_tetra *tetra)
{
	int ret;
	int	res;

	ret = 0;
	res = 0;
	map->size = size_square(tetra->nb_tetra * 4);
	map->rsize = ft_int_sqr(map->size);
	while ((ret = try_square(map, tetra, 0, 0)) != 1 && map->size <= 16 * 16)
	{
		ft_bzero(map->map, 32);
		res = ft_int_sqr(map->size) + 1;
		map->size = res * res;
		map->rsize = res;
	}
	fill_printed_map(tetra, map);
}
